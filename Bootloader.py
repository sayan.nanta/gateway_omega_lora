## For Omega2+ , lora-e5

import time
import Constants
import uuid
import os

try:
    # Open the file for writing.
    os.system('pwd')
    os.chdir("root/gateway_omega_lora")
    #os.chdir("root/rasppi_firmware")
    os.system('pwd')
except:
    pass
    
import SysLed
SysLed.led()

for i in range (0,5):
	Constants.pix0_pwr=1
	SysLed.led.run(Constants.pix0_pwr,Constants.pix1_rf,Constants.pix2_net,Constants.pix3_chrg)
	time.sleep(0.1)
	Constants.pix0_pwr=0
	SysLed.led.run(Constants.pix0_pwr,Constants.pix1_rf,Constants.pix2_net,Constants.pix3_chrg)
	time.sleep(0.1)

import subprocess
import os

import logging
import datetime
import _thread

import MqttService as MqttService
import SendData as SendData

import davinAgrox as davinAgrox
import xbee_pwr_ctl
import energy_mesure
import SendWDT
import bacup_data_service


try:
    file = open('rf_config.py', "r")
except Exception as e:
    print('rf_config Err:{}'.format(e))
    os.system('cp rf_config.bac rf_config.py')
    os.system('cat rf_config.py')
    pass

############ Generate New UUID ########################
try:
    import Constants
    import Davin_Env
    UUID = Davin_Env.UUID
    print('\n\nUUID:{}'.format(UUID))
    
except Exception as e:
    print('Err:{}'.format(e))
    import Constants
    Constants.UUID = New_uuid = str(uuid.uuid4())
    New_uuid = 'UUID="'+ New_uuid + '"\n'
    New_uuid = bytes(New_uuid,'utf-8')
    print('Generate new uuid')
    file = open('Davin_Env.py', "w+b")
    file.write(New_uuid)
    file.close()
    print('Generate sucess:{}\nReboot system'.format(New_uuid))
    time.sleep(3)
    os.system('reboot')
    pass

try:
    print('get cpuinfo')
    ls_cpuinfo = subprocess.run(['cat','/proc/cpuinfo'], stdout=subprocess.PIPE) # get last 10 line in file
    Constants.CPUINFO = ls_cpuinfo.stdout
    file = open('cpuinfo.txt', "w+b")
    file.write(ls_cpuinfo.stdout)
    file.close()
    print('*** Create file cpuinfo')
    ################ write cpu model to Constants
    ls_cpuinfo = subprocess.run(['cat','/proc/cpuinfo'], stdout=subprocess.PIPE) # get last 10 line in file
    Constants.MODEL = 'Omega2+'
    # print(Constants.MODEL)
except:
    pass

# try:
#     file = open('git_log.txt', "w+b")
#     if MachineNodename == 'pi':
#         print('Check update Firmware..')
#         retval = subprocess.run(['git','reset','--hard'], capture_output=True)
#         print(retval.stdout)
#         file.write(retval.stdout)
#         retval = subprocess.run(['git','pull'], capture_output=True)
#         print(retval.stdout)
#         file.write(retval.stdout)
#         retval = subprocess.run(['git','log','-n','1'], capture_output=True)
#         print(retval.stdout)
#         file.write(retval.stdout)
#         file.close()
# except Exception as err:
#     print('err:{}'.format(err))
#     pass
# time.sleep(10)

xbee_pwr_ctl.set_state(True)

import MqttService as MqttService
import SendData as SendData

def led_on():
    Constants.pix0_pwr=1
    SysLed.led.run(Constants.pix0_pwr,Constants.pix1_rf,Constants.pix2_net,Constants.pix3_chrg)

def led_off():
    Constants.pix0_pwr=0
    SysLed.led.run(Constants.pix0_pwr,Constants.pix1_rf,Constants.pix2_net,Constants.pix3_chrg)

def led_blink():
    time.sleep(0.5)
    led_off()
    time.sleep(0.5)
    led_on()

class main:
    def __init__(self):
        logging.info(Constants.SOFTWARE_TITLE)
        print(Constants.SOFTWARE_TITLE)

        davinAgrox.Agrox()
        time.sleep(1)
        led_blink()
		
        _thread.start_new_thread(MqttService.MqttService, ())
        time.sleep(1)

        _thread.start_new_thread(SendData.SendDataService, ())
        time.sleep(1)

        _thread.start_new_thread(davinAgrox.Agrox.agrox_run , ())

        _thread.start_new_thread(SendWDT.Send_data_wathdog, ())

        _thread.start_new_thread(bacup_data_service.BackupData, ())

        time.sleep(5)
        led_blink()
        led_blink()
        led_blink()
        energy_mesure.mesure()
        print('-Run energy_mesure')
        _thread.start_new_thread(energy_mesure.mesure.read(), ())

        print('Exit and Reboot')
        time.sleep(10)
        # os.system('reboot')
        while True:
            time.sleep(10)
        print('Exit Bootloader main')
        
        
if __name__ == '__main__':
    main()