from OmegaExpansion import relayExp
import time
import sys
import subprocess
import os

time.sleep(1)
addr = 0
print('Starting to use relay-exp functions on addr offset %d'%addr)
relayExp.setVerbosity(0)
# check initialization
#	should return 0 if the Expansion has just been plugged in
bInit   = relayExp.checkInit(addr)
if (bInit == 0):
    print ('The Relay Expansion needs to be initialized')
else:
    print ('The Relay Expansion is good to go!')
# initialize the relay-exp
ret 	= relayExp.driverInit(addr)
print("Result from relayDriverInit: %s"%ret)
if (ret != 0):
	exit()
# check initialization
#	should return 1 since the Expansion was initialized above
ret 	= relayExp.checkInit(addr)
print("checking if initialized: %s"%ret)
time.sleep(1)
print('Starting to use relay-exp functions on addr offset %d'%addr)
relayExp.setVerbosity(0)
# check initialization
#	should return 0 if the Expansion has just been plugged in
bInit   = relayExp.checkInit(addr)
if (bInit == 0):
    print ('The Relay Expansion needs to be initialized')
else:
    print ('The Relay Expansion is good to go!')
# initialize the relay-exp
ret 	= relayExp.driverInit(addr)
print("Result from relayDriverInit: %s"%ret)
if (ret != 0):
	exit()
# check initialization
#	should return 1 since the Expansion was initialized above
ret 	= relayExp.checkInit(addr)
print("checking if initialized: %s"%ret)

try:
	# if 'LED_STATUS:' in msg:
		# print('LED value :{}'.format(led_status))
	pixels0 = 1
	pixels1 = 1
	pixels2 = 1
	pixels3 = 1
	ret = relayExp.setChannel(addr, 0, pixels0) # ch0
	# time.sleep(0.05)
	ret = relayExp.setChannel(addr, 1, pixels1) # ch1
	# time.sleep(0.05)
	ret = relayExp.setChannel(addr, 2, pixels2) # ch2
	# time.sleep(0.05)
	ret = relayExp.setChannel(addr, 3, pixels3) # ch3

	ret = relayExp.setChannel(addr, 0, 1) # ch0
	# time.sleep(0.05)
	ret = relayExp.setChannel(addr, 1, 0) # ch1
	# time.sleep(0.05)
	ret = relayExp.setChannel(addr, 2, 0) # ch2
	# time.sleep(0.05)
	ret = relayExp.setChannel(addr, 3, 0) # ch3
	
	# ch = 0
	# ret = relayExp.setChannel(addr, ch, 1) # ch1
	# print("Result from relaySetChannel: %s"%ret)
	# if (ret != 0):
	# 	exit()
	# time.sleep(0.5)
	# ret = relayExp.setChannel(addr, ch, 0) # ch1
	# print("Result from relaySetChannel: %s"%ret)
	# if (ret != 0):
	# 	exit()
	# time.sleep(0.5)
except Exception as e:
	print(e)
pass
