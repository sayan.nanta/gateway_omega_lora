from OmegaExpansion import relayExp
import time
import sys
import subprocess
import os

# addr = 0
# print('Starting to use relay-exp functions on addr offset %d'%addr)
# relayExp.setVerbosity(0)
# check initialization
#	should return 0 if the Expansion has just been plugged in
# bInit   = relayExp.checkInit(addr)
# if (bInit == 0):
#     print ('The Relay Expansion needs to be initialized')
# else:
#     print ('The Relay Expansion is good to go!')
# # initialize the relay-exp
# ret 	= relayExp.driverInit(addr)
# print("Result from relayDriverInit: %s"%ret)
# if (ret != 0):
# 	exit()
# # check initialization
# #	should return 1 since the Expansion was initialized above
# ret 	= relayExp.checkInit(addr)
# print("checking if initialized: %s"%ret)

try:
	ls_led_status = subprocess.check_output(['tail','-1','/tmp/davin-info.log']) # get last 10 line in file
	# print(ls_led_status)
	# print(type(ls_led_status))
	# msg = str(ls_led_status, 'utf-8') # Byte to str
	msg = ls_led_status
	
	if 'LED_STATUS:' in msg:
		led_status = msg.split('LED_STATUS:')
		# print('1:{}'.format(led_status))
		led_status = led_status[1].replace('\n','')
		# print('2:{}'.format(led_status))
		led_status = led_status.split(',')
		# print('LED value :{}'.format(led_status))
		pixels0 = int(led_status[0],16)
		pixels1 = int(led_status[1],16)
		pixels2 = int(led_status[2],16)
		pixels3 = int(led_status[3],16)
		print('{},{},{},{}'.format(pixels0,pixels1,pixels2,pixels3))
		ret = relayExp.setChannel(0, 0, pixels0) # ch0
		# time.sleep(0.05)
		ret = relayExp.setChannel(0, 1, pixels1) # ch1
		# time.sleep(0.05)
		ret = relayExp.setChannel(0, 2, pixels2) # ch2
		# time.sleep(0.05)
		ret = relayExp.setChannel(0, 3, pixels3) # ch3
        
	# ch = 0
	# ret = relayExp.setChannel(addr, ch, 1) # ch1
	# print("Result from relaySetChannel: %s"%ret)
	# if (ret != 0):
	# 	exit()
	# time.sleep(0.5)
	# ret = relayExp.setChannel(addr, ch, 0) # ch1
	# print("Result from relaySetChannel: %s"%ret)
	# if (ret != 0):
	# 	exit()
	# time.sleep(0.5)
except Exception as e:
	print(e)
pass